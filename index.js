import Realm from "realm";
import fs from "fs"

const localConfig = {
    path: "client.realm",
};

const localRealm = await Realm.open(localConfig);
const scores = localRealm.objects("Score");

const scoresOutput = scores
    .filter((score) => score.Ruleset.OnlineID == 0 && !score.Mods.includes("RX"))
    .map((score) => {
        let metadata = score.BeatmapInfo.Metadata;
        let beatmapInfo = score.BeatmapInfo;

        return {
            artist: metadata.Artist,
            name: metadata.Title,
            diff: beatmapInfo.DifficultyName,
            stars: beatmapInfo.StarRating,
            maxCombo: score.MaxCombo,
            accuracy: score.Accuracy,
            date: score.Date,
            mods: score.Mods
        }
    })

localRealm.close();

console.log(`Number of scores: ${scoresOutput.length}`);
fs.writeFileSync("osu-scores.json", JSON.stringify(scoresOutput));
