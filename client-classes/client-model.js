export const Beatmap = {
  name: 'Beatmap',
  primaryKey: 'ID',
  properties: {
    ID: 'uuid',
    DifficultyName: 'string?',
    Ruleset: 'Ruleset',
    Difficulty: 'BeatmapDifficulty',
    Metadata: 'BeatmapMetadata',
    UserSettings: 'BeatmapUserSettings',
    BeatmapSet: 'BeatmapSet',
    Status: 'int',
    OnlineID: { type: 'int', indexed: true },
    Length: 'double',
    BPM: 'double',
    Hash: 'string?',
    StarRating: 'double',
    MD5Hash: { type: 'string?', indexed: true },
    Hidden: 'bool',
    AudioLeadIn: 'double',
    StackLeniency: 'float',
    SpecialStyle: 'bool',
    LetterboxInBreaks: 'bool',
    WidescreenStoryboard: 'bool',
    EpilepsyWarning: 'bool',
    SamplesMatchPlaybackRate: 'bool',
    DistanceSpacing: 'double',
    BeatDivisor: 'int',
    GridSize: 'int',
    TimelineZoom: 'double',
    CountdownOffset: 'int',
    OnlineMD5Hash: 'string?',
    LastLocalUpdate: 'date?',
    LastOnlineUpdate: 'date?',
    LastPlayed: 'date?',
    EditorTimestamp: 'double?'
  }
}

export const BeatmapCollection = {
  name: 'BeatmapCollection',
  primaryKey: 'ID',
  properties: {
    ID: 'uuid',
    Name: 'string?',
    BeatmapMD5Hashes: 'string?[]',
    LastModified: 'date'
  }
}

export const BeatmapDifficulty = {
  name: 'BeatmapDifficulty',
  embedded: true,
  properties: {
    DrainRate: 'float',
    CircleSize: 'float',
    OverallDifficulty: 'float',
    ApproachRate: 'float',
    SliderMultiplier: 'double',
    SliderTickRate: 'double'
  }
}

export const BeatmapMetadata = {
  name: 'BeatmapMetadata',
  properties: {
    Title: 'string?',
    TitleUnicode: 'string?',
    Artist: 'string?',
    ArtistUnicode: 'string?',
    Author: 'RealmUser',
    Source: 'string?',
    Tags: 'string?',
    PreviewTime: 'int',
    AudioFile: 'string?',
    BackgroundFile: 'string?'
  }
}

export const BeatmapSet = {
  name: 'BeatmapSet',
  primaryKey: 'ID',
  properties: {
    ID: 'uuid',
    OnlineID: { type: 'int', indexed: true },
    DateAdded: 'date',
    Beatmaps: 'Beatmap[]',
    Files: 'RealmNamedFileUsage[]',
    Status: 'int',
    DeletePending: 'bool',
    Hash: 'string?',
    Protected: 'bool',
    DateSubmitted: 'date?',
    DateRanked: 'date?'
  }
}

export const BeatmapUserSettings = {
  name: 'BeatmapUserSettings',
  embedded: true,
  properties: {
    Offset: 'double'
  }
}

export const File = {
  name: 'File',
  primaryKey: 'Hash',
  properties: {
    Hash: 'string?'
  }
}

export const KeyBinding = {
  name: 'KeyBinding',
  primaryKey: 'ID',
  properties: {
    ID: 'uuid',
    Variant: 'int?',
    Action: 'int',
    KeyCombination: 'string?',
    RulesetName: 'string?'
  }
}

export const ModPreset = {
  name: 'ModPreset',
  primaryKey: 'ID',
  properties: {
    ID: 'uuid',
    Ruleset: 'Ruleset',
    Name: 'string?',
    Description: 'string?',
    Mods: 'string?',
    DeletePending: 'bool'
  }
}

export const RealmNamedFileUsage = {
  name: 'RealmNamedFileUsage',
  embedded: true,
  properties: {
    File: 'File',
    Filename: 'string?'
  }
}

export const RealmUser = {
  name: 'RealmUser',
  embedded: true,
  properties: {
    OnlineID: 'int',
    Username: 'string?',
    CountryCode: 'string?'
  }
}

export const Ruleset = {
  name: 'Ruleset',
  primaryKey: 'ShortName',
  properties: {
    ShortName: 'string?',
    OnlineID: { type: 'int', indexed: true },
    Name: 'string?',
    InstantiationInfo: 'string?',
    Available: 'bool',
    LastAppliedDifficultyVersion: 'int'
  }
}

export const RulesetSetting = {
  name: 'RulesetSetting',
  properties: {
    RulesetName: { type: 'string?', indexed: true },
    Variant: { type: 'int', indexed: true },
    Key: 'string',
    Value: 'string'
  }
}

export const Score = {
  name: 'Score',
  primaryKey: 'ID',
  properties: {
    ID: 'uuid',
    BeatmapInfo: 'Beatmap',
    Ruleset: 'Ruleset',
    Files: 'RealmNamedFileUsage[]',
    Hash: 'string?',
    DeletePending: 'bool',
    TotalScore: 'int',
    MaxCombo: 'int',
    Accuracy: 'double',
    Date: 'date',
    PP: 'double?',
    OnlineID: { type: 'int', indexed: true },
    User: 'RealmUser',
    Mods: 'string?',
    Statistics: 'string?',
    Rank: 'int',
    Combo: 'int',
    MaximumStatistics: 'string?',
    BeatmapHash: 'string?',
    IsLegacyScore: 'bool'
  }
}

export const Skin = {
  name: 'Skin',
  primaryKey: 'ID',
  properties: {
    ID: 'uuid',
    Name: 'string?',
    Creator: 'string?',
    InstantiationInfo: 'string?',
    Hash: 'string?',
    Protected: 'bool',
    Files: 'RealmNamedFileUsage[]',
    DeletePending: 'bool'
  }
}

