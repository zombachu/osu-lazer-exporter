import fs from "fs"

const data = fs.readFileSync("osu-scores.json");
const scores = JSON.parse(data);

// console.log(scores);

function getFirstPassOfStars(stars, acc = 0) {
    return scores
        .filter((score) => 
            score.accuracy >= acc 
            && score.stars >= stars 
            && score.stars < stars + 1)
        .sort((score1, score2) => score1.date < score2.date)[0];
}

function printPass(stars, acc, grade = "") {
    console.log(`First ${stars}*${grade}: ${JSON.stringify(getFirstPassOfStars(stars, acc))}`);
}

// yanderedevass code cause I'm lazy
for (let stars = 0; stars <= 9; stars++) {
    printPass(stars, 0);
    printPass(stars, 0.7, " C");
    printPass(stars, 0.8, " B");
    printPass(stars, 0.9, " A");
    printPass(stars, 0.95, " S");
}
